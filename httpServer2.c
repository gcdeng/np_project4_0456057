#include <arpa/inet.h>		//For htonl(), htons().
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>			//For open().
#include <sys/types.h>		//For socket(), bind(), accept, semaphore, shared memory.
#include <sys/socket.h>		//For socket(), bind(), listen(), accept().
#include <sys/wait.h>		//For wait().
#include <sys/stat.h>		//For open().
#include <sys/time.h>		//For timeval.
#include <sys/ipc.h>		//For semaphore, shared memory.
#include <sys/sem.h>		//For semaphore.
#include <sys/shm.h>		//For shared memory.
#include <sys/errno.h>
#include <sys/select.h>		//For select().
#include <fcntl.h>			//For open().
#include <errno.h>			//For errno.
#include <stdlib.h>			//For exit(), setenv().
#include <unistd.h>			//For close(), read(), dup(), access().
#include <iostream>			//For cout.
#include <string.h>			//For bzero(), strspn(), strpbrk().
#include <signal.h>			//For signal().
#include <list>				  //For list.
#include <ctype.h>
#include <sys/uio.h>
#include <fcntl.h>

#define MAX_MSG 100000

char *root;
int listenfd, clientfd;

void startServer(char *port){
  struct addrinfo hints;
  struct addrinfo *servinfo;
  struct addrinfo *p;
  int status;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  if((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0 ){
    printf("getaddrinfo() error\n");
    exit(EXIT_FAILURE);
  }

  for(p=servinfo; p!=NULL; p=p->ai_next){
    listenfd = socket(p->ai_family, p->ai_socktype, 0);
    if(listenfd == -1) continue;
    if(bind(listenfd, p->ai_addr, p->ai_addrlen) == 0) {
      break;
    } else {
      printf("port biding\n");
      exit(EXIT_FAILURE);
    }
  }

  freeaddrinfo(servinfo);

  if(listen(listenfd, 20) != 0){
    printf("listen() error\n");
    exit(EXIT_FAILURE);
  }
}

void responseClient(void) {
  char headersmsg[MAX_MSG];
  char filePath[MAX_MSG];
  char fileData[MAX_MSG];
  char *get, *fileName, *QUERY_STRING;
  int recvStatus;
  int fPtr, readCount;

  memset(headersmsg, 0, MAX_MSG);
  memset(filePath, 0, MAX_MSG);
  memset(fileData, 0, MAX_MSG);
  recvStatus = recv(clientfd, headersmsg, MAX_MSG, 0);
  if (recvStatus < 0) // receive error
    printf("recv() error\n");
  else if (recvStatus == 0) // receive socket closed
    printf("Client disconnected!\n");
  else { // headers messages received
    printf("[Headers]:\n%s", headersmsg);
    get = strtok(headersmsg, " \t\n"); //GET method
    fileName = strtok(NULL, " \t");
    printf("[Workflow]:\n");
    printf("extension is .html?: %s\n", &fileName[strlen(fileName)-4]);
    if(strcmp(&fileName[strlen(fileName)-4], "html") == 0){ // is html file
      strcpy(filePath, root);
      strcpy(&filePath[strlen(root)], fileName); //set file path
      printf("filePath: %s\n", filePath);
      if( (fPtr = open(filePath, O_RDONLY)) != -1 ){
         send(clientfd, "HTTP/1.0 200 OK\n\n", strlen("HTTP/1.0 200 OK\n\n"), 0); // \n\n is necessary!!!
         while ( (readCount = read(fPtr, fileData, 1024)) > 0 ) {
           write(clientfd, fileData, readCount);
         }
      } else {
        write(clientfd, "HTTP/1.0 404 Not Found\n", strlen("HTTP/1.0 404 Not Found\n")); //FILE NOT FOUND
      }
    }
    else { //maybe it is cgi file or not found
      // write(clientfd, headersmsg, strlen(headersmsg));
      fileName = strtok(fileName, "?"); // remove string after '?'
      printf("fileName: %s\n", fileName);
      printf("extension is .cgi?: %s\n", &fileName[strlen(fileName)-3]);
      if (strcmp(&fileName[strlen(fileName)-3], "cgi") == 0){ // is .cgi file
        strcpy(filePath, root);
        strcpy(&filePath[strlen(root)], fileName); // set file path
        printf("filePath: %s\n", filePath);

        // if file is hw4.cgi then setenv variable
        if(strncmp(fileName, "/hw4.cgi", strlen("/hw4.cgi")) == 0){
          QUERY_STRING = strtok(NULL, " \n"); //get string after ? it is QUERY_STRING
          printf("QUERY_STRING: %s\n", QUERY_STRING);
          if(setenv("QUERY_STRING", QUERY_STRING, 1)<0) printf("setenv QUERY_STRING error\n");
          setenv("CONTENT_LENGTH", "NP ", 1);
          setenv("REQUEST_METHOD", "GET ", 1);
          setenv("SCRIPT_NAME", "NP ", 1);
          setenv("REMOTE_HOST", "NP ", 1);
          setenv("REMOTE_ADDR", "NP ", 1);
          setenv("AUTH_TYPE", "NP ", 1);
          setenv("REMOTE_USER", "EricDeng ", 1);
          setenv("REMOTE_IDENT", "NP ", 1);
        }

        send(clientfd, "HTTP/1.0 200 OK\n", strlen("HTTP/1.0 200 OK\n"), 0); // \n is necessary!!!

        int childpid = fork();
        if(childpid<0){
          printf("cgi fork() error\n");
        } else if(childpid==0){
          printf("cgi child\n\n");
          dup2(clientfd, STDOUT_FILENO);
          if(execl(filePath, fileName, (char *)NULL)==-1){
            printf("execl() cgi error\n");
            exit(EXIT_FAILURE);
          }
        } else {
          printf("cgi parent\n");
          wait(&childpid);
          exit(EXIT_FAILURE);
        }
      } else { //not html, cgi file
        write(clientfd, "HTTP/1.0 404 Not Found\n", strlen("HTTP/1.0 404 Not Found\n")); //FILE NOT FOUND
      }
    }
  }
  shutdown(clientfd, SHUT_RDWR);
  close(clientfd);
  clientfd = -1;
}

int main(int argc, char* argv[]){
  char alg; //command line arguments
  char port[5];
  strcpy(port, "5566"); //preset port = 5566
  root = getenv("PWD"); //preset root = pwd (Print Working Directory)

  while((alg = getopt(argc, argv, "p:")) != -1){
    switch(alg){
      case 'p': //set port
        strcpy(port, optarg);
        break;
      default:
        exit(EXIT_FAILURE);
    }
  }
  printf("Server is started, port = %s, root = %s\n", port, root);
  startServer(port);

  struct sockaddr_in clientaddr;
  socklen_t addrlen;

  while(1){
    addrlen = sizeof(clientaddr);
    clientfd = accept(listenfd, (struct sockaddr *)&clientaddr, &addrlen);
    if(clientfd < 0) {
      printf("accept() error\n");
    } else {
      if(fork() == 0){
        //child process
        responseClient();
        exit(EXIT_FAILURE);
      }
      else {
        // parent process
        close(clientfd);
        // exit(EXIT_FAILURE);
        continue;
      }
    }
  }
}
