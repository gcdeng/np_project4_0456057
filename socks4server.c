#include <arpa/inet.h>		//For htonl(), htons().
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>			//For open().
#include <sys/types.h>		//For socket(), bind(), accept, semaphore, shared memory.
#include <sys/socket.h>		//For socket(), bind(), listen(), accept().
#include <sys/wait.h>		//For wait().
#include <sys/stat.h>		//For open().
#include <sys/time.h>		//For timeval.
#include <sys/ipc.h>		//For semaphore, shared memory.
#include <sys/sem.h>		//For semaphore.
#include <sys/shm.h>		//For shared memory.
#include <sys/errno.h>
#include <sys/select.h>		//For select().
#include <fcntl.h>			//For open().
#include <errno.h>			//For errno.
#include <stdlib.h>			//For exit(), setenv().
#include <unistd.h>			//For close(), read(), dup(), access().
#include <iostream>			//For cout.
#include <string.h>			//For bzero(), strspn(), strpbrk().
#include <signal.h>			//For signal().
#include <list>				  //For list.
#include <ctype.h>
#include <sys/uio.h>
#include <fcntl.h>

#define MAX_MSG 100000

char *root;
// master sock and slave sock
int listenfd, clientfd;

void startServer(char *port){
  struct addrinfo hints;
  struct addrinfo *servinfo;
  struct addrinfo *p;
  int status;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE;

  if((status = getaddrinfo(NULL, port, &hints, &servinfo)) != 0 ){
    printf("[getaddrinfo() error]\n");
    exit(EXIT_FAILURE);
  }

  for(p=servinfo; p!=NULL; p=p->ai_next){
    listenfd = socket(p->ai_family, p->ai_socktype, 0);
    if(listenfd == -1) continue;
    if(bind(listenfd, p->ai_addr, p->ai_addrlen) == 0) {
      break;
    } else {
      printf("[port biding]\n");
      exit(EXIT_FAILURE);
    }
  }

  freeaddrinfo(servinfo);

  if(listen(listenfd, 20) != 0){
    printf("[listen() error]\n");
    exit(EXIT_FAILURE);
  }
}

int connect_server(char *ip, int port){
  //create DEST Addr socket
  int web_fd;
  struct sockaddr_in web_server_addr; //an Internet endpoint address
  struct hostent *phe; //pointer to host information entry

  if((phe = gethostbyname(ip)) == NULL){
    printf("[gethostbyname error]\n");
    exit(EXIT_FAILURE);
  }

  memset(&web_server_addr, 0, sizeof(web_server_addr));
  web_server_addr.sin_family = AF_INET;
  web_server_addr.sin_addr = *((struct in_addr *)phe->h_addr);;
  web_server_addr.sin_port = htons(port);

  web_fd = socket(AF_INET, SOCK_STREAM, 0);
  printf("[create socket]\n");

  if(connect(web_fd, (struct sockaddr *)&web_server_addr, sizeof(web_server_addr)) < 0){
    printf("[Connect failed]\n");
    exit(EXIT_FAILURE);
  }
  else{
    printf("[Connect web server success]\n");
  }

  return web_fd;
}

void socks_server(void) {
  unsigned char request[MAX_MSG];
  unsigned char reply[8];
  // get request
  read(clientfd, request, MAX_MSG);
  printf("[Read request]\n");
  // set request packet format
  unsigned char VN = request[0]; //version number
  unsigned char CD = request[1]; //command 1:connect mode, 2:bind mode
  unsigned int DEST_PORT = request[2] << 8 | request[3]; //destination port
  unsigned int DEST_IP = request[4] << 24 | request[5] << 16 | request[6] << 8 | request[7]; //destination ip
  unsigned char *USER_ID = request + 8;
  // variable for firewall
  FILE *firewall_fd;
  char rule[10];
  char mode[10];
  char address_string[20];
  unsigned char address[4];
  char *pch;

  //check only Socks4 can access
  // printf("VN: %d\n", VN);
  // printf("CD: %d\n", CD);
  // printf("DEST_PORT: %d\n", DEST_PORT);
  if(VN != 0x04) {
    printf("[Is not socks4]\n");
    exit(EXIT_FAILURE);
  }
  // Firewall setting
  if( (firewall_fd = fopen("socks.conf", "r")) == NULL )
    printf("[fail to read socks.conf]\n");

  while(!feof(firewall_fd)){
    fscanf(firewall_fd, "%s %s %s", rule, mode, address_string);
    // printf("mode: %s\n", mode);
    pch = strtok(address_string, ".");
    address[0] = (unsigned char)atoi(pch);
    pch = strtok(NULL, ".");
    address[1] = (unsigned char)atoi(pch);
    pch = strtok(NULL, ".");
    address[2] = (unsigned char)atoi(pch);
    pch = strtok(NULL, ".");
    address[3] = (unsigned char)atoi(pch);

    if( (!strcmp(mode, "c") && CD==0x01) || (!strcmp(mode, "b") && CD==0x02)){
      // printf("match mode: %s %s %u.%u.%u.%u\n",rule, mode, address[0], address[1], address[2], address[3] );
      if( ((address[0] == request[4]) || (address[0] == 0x00)) && ((address[1] == request[5]) || (address[1] == 0x00))
      && ((address[2] == request[6]) || (address[2] == 0x00)) && ((address[3] == request[7]) || (address[3] == 0x00)) ){
        // printf("[Firewall Rule Accept]\n");
        reply[1] = 0x5A; //90 request granted
        break;
      }
    }
  }
  // hard code for NCTU firewall
  // if( ((unsigned char)(140) == request[4]) && ((unsigned char)(113) == request[5]) ){
  //   // printf("[Firewall Rule Accept]\n");
  //   reply[1] = 0x5A; //90 request granted
  // } else {
  //   reply[1] = 0x5B;
  // }

  //print result
  printf("==================================\n");
  printf("VN: %u, CD: %u (%s), USER_ID: %s\n", VN, CD, (CD==0x01)?"Connect mode":"Bind mode", USER_ID);
  printf("DEST = %u.%u.%u.%u:%u\n", request[4], request[5], request[6], request[7], DEST_PORT);
  printf("Firewall Rule = %s.\n", (reply[1] == 0x5A)?"Accept":"Reject");
  if(reply[1] == 0x5A)
    printf("Pass Rule: %s %s %u.%u.%u.%u\n", rule, mode, address[0], address[1], address[2], address[3]);
  else
    printf("[Can't Pass Rule]\n");
  printf("==================================\n");

  //Connect mode
  if (CD == 0x01) {
    printf("SOCKS_CONNECT MODE GRANTED...\n");
    // set reply packet format
    reply[0] = 0;
    // reply[1] = 0x5A;
    reply[2] = request[2];
    reply[3] = request[3];
    reply[4] = request[4];
    reply[5] = request[5];
    reply[6] = request[6];
    reply[7] = request[7];
    //reply client
    write(clientfd, reply, 8);
    printf("[Reply client]\n");

    // get IP string
    char ip[20];
    sprintf(ip, "%u.%u.%u.%u", request[4], request[5], request[6], request[7]);

    // connect to request server
    int webServerFd = connect_server(ip, DEST_PORT);

    // read & write message between client(browser) and server
    fd_set afds, rfds;
    int nfds = ((clientfd<webServerFd)? webServerFd: clientfd)+1;
    int browser_read_end = 0;
    int webserver_read_end = 0;
    int readLength;
    int writeLength;
    char msg_buf[MAX_MSG];
    FD_ZERO(&afds);
    FD_SET(webServerFd, &afds);
    FD_SET(clientfd, &afds);

    while(1){
      FD_ZERO(&rfds);
      memcpy(&rfds, &afds, sizeof(rfds));
      if(select(nfds, &rfds, NULL, NULL, NULL)<0){
        printf("[select failed]\n");
        exit(EXIT_FAILURE);
      }
      // read from browser, write to server
      if(FD_ISSET(clientfd, &rfds)){
        memset(msg_buf, '\0', MAX_MSG);
        readLength = read(clientfd, msg_buf, MAX_MSG);
        if(readLength == 0){
          printf("[Browser read end]\n");
          FD_CLR(clientfd, &afds);
          browser_read_end=1;
        }
        else if(readLength == -1){
          printf("[Browser read error]\n");
          FD_CLR(clientfd, &afds);
          browser_read_end=1;
        }
        else{
          writeLength = write(webServerFd, msg_buf, readLength);
          // printf("read length: %d\n", readLength);
          // printf("write length: %d\n", writeLength);
          // printf("msg_buf: %s\n", msg_buf);
        }
      }
      // read from server, write to browser
      if(FD_ISSET(webServerFd, &rfds)){
        memset(msg_buf, '\0', MAX_MSG);
        readLength = read(webServerFd, msg_buf, MAX_MSG);
        if(readLength == 0){
          printf("[Server read end]\n");
          FD_CLR(webServerFd, &afds);
          webserver_read_end=1;
        }
        else if(readLength == -1){
          printf("[Server read error]\n");
          FD_CLR(webServerFd, &afds);
          webserver_read_end=1;
        }
        else{
          writeLength = write(clientfd, msg_buf, readLength);
          // printf("read length: %d\n", readLength);
          // printf("write length: %d\n", writeLength);
          // printf("msg_buf: %s\n", msg_buf);
        }
      }
      // read end and write end
      if(webserver_read_end && browser_read_end) break;
    }
  }
  // Bind mode
  else if (CD == 0x02) {
    printf("SOCKS_BIND MODE GRANTED...\n");
    int bindfd;
    static struct sockaddr_in bind_addr; //an Internet endpoint address

    // socket
    if((bindfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
      printf("[Bind mode create socket error]\n");
      exit(EXIT_FAILURE);
    } else printf("[Bind mode create socket success]\n");

    memset(&bind_addr, 0, sizeof(bind_addr));       /* Clear struct */
    bind_addr.sin_family = AF_INET;
    bind_addr.sin_addr.s_addr = htonl(INADDR_ANY); //any ip
    bind_addr.sin_port = htons(INADDR_ANY); // any port

    // bind
    if((bind(bindfd, (struct sockaddr *)&bind_addr, sizeof(bind_addr))) < 0){
      printf("[Bind mode bind port failed]\n");
      exit(EXIT_FAILURE);
    } else printf("[Bind mode bind port success]\n");

    // getsockname
    struct sockaddr_in serv, guest;
    socklen_t serv_len = sizeof(serv);
    // socklen_t guest_len = sizeof(guest);
    // char serv_ip[20];
    // char guest_ip[20];
    if(getsockname(bindfd, (struct sockaddr *)&serv, &serv_len) < 0){
      printf("[getsockname failed]\n");
    } else printf("[getsockname success]\n");
    // if(getpeername(bindfd, (struct sockaddr *)&guest, &guest_len) < 0){
    //   printf("[getpeername failed]\n");
    // } else printf("[getpeername success]\n");
    // inet_ntop(AF_INET, &serv.sin_addr, serv_ip, sizeof(serv_ip));
    // inet_ntop(AF_INET, &guest.sin_addr, guest_ip, sizeof(guest_ip));
    // printf("host %s:%d guest %s:%d\n", serv_ip, ntohs(serv.sin_port), guest_ip, ntohs(guest.sin_port));

    // listen
    if(listen(bindfd, 5) < 0) {
      printf("[Bind mode listen failed]\n");
      exit(EXIT_FAILURE);
    } else printf("[Bind mode listen success]\n");

    // set reply packet format
    reply[0] = 0;
    // reply[1] = 0x5A;
    reply[2] = (unsigned char)(ntohs(serv.sin_port)/256);
    reply[3] = (unsigned char)(ntohs(serv.sin_port)%256);
    reply[4] = 0;
    reply[5] = 0;
    reply[6] = 0;
    reply[7] = 0;
    //reply client
    write(clientfd, reply, 8);
    printf("[Reply client]\n");

    int ftp_fd;
    struct sockaddr_in ftp_addr;
    socklen_t ftpaddr_len = sizeof(ftp_addr);
    // printf("ftp_fd: %d, bindfd: %d, ftpaddr_len: %d\n", ftp_fd, bindfd, (int)ftpaddr_len);
    ftp_fd = accept(bindfd, (struct sockaddr *)&ftp_addr, &ftpaddr_len);
    // printf("ftp_fd: %d\n", ftp_fd);
    if(ftp_fd < 0){
      printf("[Bind mode accept failed]\n");
      exit(EXIT_FAILURE);
    } else printf("[Bind mode accept success]\n");

    //reply client again !!!important!!!
    write(clientfd, reply, 8);
    printf("[Reply client again!]\n");

    fd_set afds, rfds;
    int nfds = ((clientfd<ftp_fd)? ftp_fd: clientfd)+1;
    int browser_read_end = 0;
    int ftp_read_end = 0;
    int readLength;
    int writeLength;
    char msg_buf[MAX_MSG];
    FD_ZERO(&afds);
    FD_SET(ftp_fd, &afds);
    FD_SET(clientfd, &afds);

    while(1){
      FD_ZERO(&rfds);
      memcpy(&rfds, &afds, sizeof(rfds));
      if(select(nfds, &rfds, NULL, NULL, NULL)<0){
        printf("[select failed]\n");
        exit(EXIT_FAILURE);
      }
      // read from browser, write to server
      if(FD_ISSET(clientfd, &rfds)){
        memset(msg_buf, '\0', MAX_MSG);
        readLength = read(clientfd, msg_buf, MAX_MSG);
        if(readLength == 0){
          printf("[Client read end]\n");
          FD_CLR(clientfd, &afds);
          browser_read_end=1;
        }
        else if(readLength == -1){
          printf("[Client read error]\n");
          FD_CLR(clientfd, &afds);
          browser_read_end=1;
        }
        else{
          writeLength = write(ftp_fd, msg_buf, readLength);
          // printf("read length: %d\n", readLength);
          // printf("write length: %d\n", writeLength);
          // printf("msg_buf: %s\n", msg_buf);
        }
      }
      // read from server, write to browser
      if(FD_ISSET(ftp_fd, &rfds)){
        memset(msg_buf, '\0', MAX_MSG);
        readLength = read(ftp_fd, msg_buf, MAX_MSG);
        if(readLength == 0){
          printf("[FTP read end]\n");
          FD_CLR(ftp_fd, &afds);
          ftp_read_end=1;
        }
        else if(readLength == -1){
          printf("[FTP read error]\n");
          FD_CLR(ftp_fd, &afds);
          ftp_read_end=1;
        }
        else{
          writeLength = write(clientfd, msg_buf, readLength);
          // printf("read length: %d\n", readLength);
          // printf("write length: %d\n", writeLength);
          // printf("msg_buf: %s\n", msg_buf);
        }
      }
      // read end and write end
      if(ftp_read_end && browser_read_end) break;
    }
  }
}

int main(int argc, char* argv[]){
  char alg; //command line arguments
  char port[5];
  strcpy(port, "5566"); //preset port = 5566
  root = getenv("PWD"); //preset root = pwd (Print Working Directory)

  while((alg = getopt(argc, argv, "p:")) != -1){
    switch(alg){
      case 'p': //set port
        strcpy(port, optarg);
        break;
      default:
        exit(EXIT_FAILURE);
    }
  }
  printf("Server is started, [Port] = %s, [Root] = %s\n", port, root);
  // start a server
  startServer(port);

  struct sockaddr_in clientaddr;
  socklen_t addrlen;

  while(1){
    addrlen = sizeof(clientaddr);
    // accept
    clientfd = accept(listenfd, (struct sockaddr *)&clientaddr, &addrlen);
    if(clientfd < 0) {
      printf("[accept() error]\n");
    } else {
      if(fork() == 0){
        //child process
        printf("SRC = %s:%u\n", inet_ntoa(clientaddr.sin_addr), clientaddr.sin_port);
        socks_server();
        exit(EXIT_FAILURE);
      }
      else {
        // parent process
        close(clientfd);
        // exit(EXIT_FAILURE);
        continue;
      }
    }
  }
}
