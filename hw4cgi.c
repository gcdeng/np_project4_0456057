#include <arpa/inet.h>		//For htonl(), htons().
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>			//For open().
#include <sys/types.h>		//For socket(), bind(), accept, semaphore, shared memory.
#include <sys/socket.h>		//For socket(), bind(), listen(), accept().
#include <sys/wait.h>		//For wait().
#include <sys/stat.h>		//For open().
#include <sys/time.h>		//For timeval.
#include <sys/ipc.h>		//For semaphore, shared memory.
#include <sys/sem.h>		//For semaphore.
#include <sys/shm.h>		//For shared memory.
#include <sys/errno.h>
#include <sys/select.h>		//For select().
#include <fcntl.h>			//For open().
#include <errno.h>			//For errno.
#include <stdlib.h>			//For exit(), setenv().
#include <unistd.h>			//For close(), read(), dup(), access().
#include <iostream>			//For cout.
#include <string.h>			//For bzero(), strspn(), strpbrk().
#include <signal.h>			//For signal().
#include <list>				  //For list.
#include <ctype.h>
#include <sys/uio.h>
#include <fcntl.h>

#define MAX_SERVER 5
#define MAX_CLIENT 500
#define MAX_MESSAGE 100000

#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3

int writeEnable[MAX_SERVER] = {0};

typedef struct server{
  char ip[20];
  int port;
  char fileName[20];
  int csock;
  FILE *fPtr;
  int tdID;
  char msg_buf[MAX_MESSAGE];
  char ssIP[20]; //socks server IP
  int ssPort; //socks server Port
} Server;

void initialize_server(Server *server){
  strcpy(server->ip, "");
  strcpy(server->ssIP, "");
  server->port = 0;
  server->ssPort = 0;
  strcpy(server->fileName, "");
  // server->csock = 0;
  // server->tdID = 0;
  strcpy(server->msg_buf, "");
}

// replace char to string in a string (use from google)
char *str_replace (char *source, char *find,  char *rep){
   int find_L=strlen(find);
   int rep_L=strlen(rep);
   int length=strlen(source)+1;
   int gap=0;

   char *result = (char*)malloc(sizeof(char) * length);
   strcpy(result, source);

   char *former=source;
   char *location= strstr(former, find);

   while(location!=NULL){
       gap+=(location - former);
       result[gap]='\0';

       length+=(rep_L-find_L);
       result = (char*)realloc(result, length * sizeof(char));
       strcat(result, rep);
       gap+=rep_L;

       former=location+find_L;
       strcat(result, former);

       location= strstr(former, find);
   }
   return result;
}

/***
* params tdID, csock
* print read result
* return read length
*/
int receiveMsg(int m, int socket){
  char buf[MAX_MESSAGE], *tmp, line[MAX_MESSAGE];
  int len;

  len = read(socket, buf, sizeof(buf)-1);
  if(len < 0) return -1;

  buf[len] = 0; // keypoint to reset buf

  if(len > 0){
    for(tmp = strtok(buf, "\n"); tmp; tmp = strtok(NULL, "\n")){
      if ( !strncmp(tmp, "% ", 2) ){
        writeEnable[m] = 1;
      }
      tmp = str_replace(tmp, "<", "&lt;");
      tmp = str_replace(tmp, ">", "&gt;");

      memset(line, 0, strlen(line));
      memcpy(line, tmp, strlen(tmp));
      for(int i=0; i<=strlen(line); i++){ //delete \r\n
        if(line[i] == '\r') line[i] = '\0';
        if(line[i] == '\n') line[i] = '\0';
      }
      // printf("<br>%s", line);  // echo input
      if(strcmp(line, "****************************************") == 0){
        printf("<script>document.all['m%d'].innerHTML += \"%s\";</script>\r\n", m, line);
      } else if(strcmp(line, "** Welcome to the information server. **") == 0){
        printf("<script>document.all['m%d'].innerHTML += \"<br>%s<br>\";</script>\r\n", m, line);
      } else {
        printf("<script>document.all['m%d'].innerHTML += \"<br>%s\";</script>\r\n", m, line);
      }
    }
  }
  fflush(stdout);
  return len;
}

/***
* read a command line from test data file
* return length of line command
*/
int readline(int fd, char *ptr, int maxlen){
  int n, rc;
  char c;
  *ptr = 0;
  for(n=1; n<maxlen; n++){
    rc=read(fd,&c,1);
    if(rc== 1){
      *ptr++ = c;
      if(c=='\n')
        break;
    } else if(rc==0){
      if(n==1)
        return 0;
      else
        break;
    } else return(-1);
  }
  return n;
}

int requestServer(Server servers[]){
  fd_set rfds, wfds, rs, ws;
  // FILE *fPtr;
  FD_ZERO(&rfds);
  FD_ZERO(&wfds);
  FD_ZERO(&rs);
  FD_ZERO(&ws);

  struct hostent *phe; //pointer to host information entry
  struct sockaddr_in addr; //an Internet endpoint address

  int nfds;
  int flag;
  int SERVER_PORT;
  int unsend[MAX_SERVER];
  int needWrite[MAX_SERVER];
  int status[MAX_SERVER];
  int readLineLen = 0;
  int connectCount = 0;
  int readCount = 0;
  int writeCount = 0;
  int error = 0;

  for(int i = 0; i < MAX_SERVER; i++){
    if(strcmp(servers[i].ip, "") != 0){
      // handle parameters
      servers[i].fPtr = fopen(servers[i].fileName, "r");
      if((phe = gethostbyname(servers[i].ssIP)) == NULL){ //socks server IP
        printf("gethostbyname error\n");
        exit(EXIT_FAILURE);
      }
      SERVER_PORT = servers[i].port; //ras server port

      // create socket
      servers[i].csock = socket(AF_INET, SOCK_STREAM, 0);
      printf("[create socket]\n");

      memset(&addr, 0, sizeof(addr));
      addr.sin_family = AF_INET;
      addr.sin_port = htons(servers[i].ssPort); //socks server port
      addr.sin_addr = *((struct in_addr *)phe->h_addr);

      // establish connect to socks server
      if(connect(servers[i].csock, (struct sockaddr *)&addr, sizeof(addr)) <0 ){
        if(errno != EINPROGRESS)
          printf("[connect failed]\n");
      } else{
        printf("[connect sucess]\n");
      }

      // non-blocking setting
      flag = fcntl(servers[i].csock, F_GETFL, 0);
      fcntl(servers[i].csock, F_SETFL, flag | O_NONBLOCK);
      printf("[fcntl]\n");

      /*** make socks protocol package ***/
      unsigned char request[MAX_MESSAGE];
      unsigned char reply[8];
      memset(request, '\0', MAX_MESSAGE);
      request[0] = 0x04; //VN
      request[1] = 0x01; //CD
      // DEST_PORT
      request[2] = (unsigned char)(SERVER_PORT/256);
      request[3] = (unsigned char)(SERVER_PORT%256);
      // DEST_IP
      char *pch;
      pch = strtok(servers[i].ip, ".");
      request[4] = (unsigned char)atoi(pch);
      pch = strtok(NULL, ".");
      request[5] = (unsigned char)atoi(pch);
      pch = strtok(NULL, ".");
      request[6] = (unsigned char)atoi(pch);
      pch = strtok(NULL, ".");
      request[7] = (unsigned char)atoi(pch);
      // send request to socks
      write(servers[i].csock, request, MAX_MESSAGE);
      // receive reply from socks
      sleep(1);
      if(read(servers[i].csock, reply, 8)<0){
        printf("[Read socks reply error!]\n");
        exit(EXIT_FAILURE);
      } else printf("[Read socks reply success]\n");
      // request granted or not
      if(reply[1] != 0x5A) {
        printf("[reply[1]:%u Request rejected or failed!]\n", reply[1]);
        exit(EXIT_FAILURE);
      } else printf("[reply[1]:%u Request granted]\n", reply[1]);
      /*** socks part end ***/

      // handle fds
      nfds = MAX_CLIENT+5;
      // nfds = FD_SETSIZE;
      FD_SET(servers[i].csock, &rs);
      // FD_SET(csock, &ws);
      FD_SET(fileno(servers[i].fPtr),&rs);

      // set number of connect server
      connectCount++;
      // set status of every servers
      status[i] = F_CONNECTING;
      // initial is no data need to send
      unsend[i] = 0;
      // initial write is disable
      writeEnable[i] = 0;
      // set tdID of each server
      servers[i].tdID = i;
    }
  }

  while(connectCount > 0){
    memcpy(&rfds, &rs, sizeof(rfds));
    memcpy(&wfds, &ws, sizeof(wfds));

    if(select(nfds, &rfds, &wfds, NULL, NULL) < 0) {
      printf("[select() failed]\n");
      exit(EXIT_FAILURE);
    }

    // connecting
    for(int i = 0; i < MAX_SERVER; i++){
      if(strcmp(servers[i].ip, "") != 0){
        if(status[i] == F_CONNECTING && (FD_ISSET(servers[i].csock, &rfds) || FD_ISSET(servers[i].csock, &wfds))) {
          status[i] = F_READING; //read first then write, read...
        }
      }
    }

    // receive message from server
    for(int i = 0; i < MAX_SERVER; i++){
      if(strcmp(servers[i].ip, "") != 0){
        if(status[i] == F_READING && FD_ISSET(servers[i].csock, &rfds)){
          // printf("[readfd]\n");
          while((readCount = receiveMsg(servers[i].tdID, servers[i].csock)) > 0);
          if(readCount <= 0){
            // read finished
            // printf("<br>[readfd finished]\n");
            status[i] = F_WRITING;
            FD_CLR(servers[i].csock, &rs);
            FD_SET(servers[i].csock, &ws);
          }
        }
      }
    }

    // read command line from test data file
    for(int i = 0; i < MAX_SERVER; i++){
      if(strcmp(servers[i].ip, "") != 0){
        if(!unsend[i] && FD_ISSET(fileno(servers[i].fPtr), &rfds)){
          readLineLen = readline(fileno(servers[i].fPtr), servers[i].msg_buf, sizeof(servers[i].msg_buf));
          servers[i].msg_buf[readLineLen] = 0;
          needWrite[i] = readLineLen;
          if (needWrite[i] < 0) {
            exit(EXIT_FAILURE);
          } else if (needWrite[i] == 0) { //EOF
            FD_CLR(fileno(servers[i].fPtr), &rfds);
            close(fileno(servers[i].fPtr));
          } else {
            unsend[i] = 1;
          }
        }
      }
    }

    // write command line to server
    for(int i = 0; i < MAX_SERVER; i++){
      if(strcmp(servers[i].ip, "") != 0){
        if(unsend[i] && status[i] == F_WRITING && FD_ISSET(servers[i].csock, &wfds)){
          if (writeEnable[i]) {
            // printf("[writefd]\n");
            writeCount = write(servers[i].csock, servers[i].msg_buf, strlen(servers[i].msg_buf));
            //waiting for server messages
            sleep(1);
            needWrite[i] -= writeCount;

            writeEnable[i] = 0; //*** if write unsuccess

            // print command of test data
            for(int j = 0; j <= strlen(servers[i].msg_buf); j++){ //delete \r\n
              if(servers[i].msg_buf[j] == '\r') servers[i].msg_buf[j] = '\0';
              if(servers[i].msg_buf[j] == '\n') servers[i].msg_buf[j] = '\0';
            }
            printf("<b>");
            printf("<script>document.all['m%d'].innerHTML += \"<b>%s</b>\";</script>\r\n", servers[i].tdID, servers[i].msg_buf);
            printf("</b>");

            if(!strncmp(servers[i].msg_buf, "exit", 4)){ //exit
              while(receiveMsg(servers[i].tdID, servers[i].csock)>0);
              printf("[write finished]\n");
              status[i] = F_DONE;
              FD_CLR(servers[i].csock, &ws);
              FD_CLR(fileno(servers[i].fPtr), &rs);
              connectCount--;
            }

            if(writeCount <= 0 || needWrite[i] <= 0){ //write finished, back to read
              status[i] = F_READING;
              unsend[i] = 0;
              FD_CLR(servers[i].csock, &ws);
              FD_SET(servers[i].csock, &rs);
            }
          } else { // writeEnable[i] == 0, can't write, back to read
            status[i] = F_READING;
            unsend[i] = 1;
            FD_CLR(servers[i].csock, &ws);
            FD_SET(servers[i].csock, &rs);
          }
        }
      }
    }
  }
}

void responseServer(Server servers[]){
  for (int i = 0; i < MAX_SERVER; i++) {
    printf("IP: %s, Port: %d, File Name: %s<br/>", servers[i].ip, servers[i].port, servers[i].fileName);
  }
  int mNumber[MAX_SERVER];
  printf("<!DOCTYPE html>\r\n");
  printf("<html>\r\n");
  printf("<head>\r\n");
  printf("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\"/>\r\n");
  printf("<meta http-equiv=\"encoding\" content=\"big5\"/>\r\n");
  printf("<title>Network Programming Homework 3</title>\r\n");
  printf("</head>\r\n");
  printf("<body bgcolor=#336699>\r\n");
  printf("<font face=\"Courier New\" size=2 color=#FFFF99>\r\n");
  printf("<table width=\"800\" border=\"1\">\r\n");
  // IP td
  printf("<tr>\r\n");
  for (int i = 0; i < MAX_SERVER; i++) {
    if(strcmp(servers[i].ip, "") != 0){
      printf("<td>%s</td>", servers[i].ip);
      mNumber[i] = 1;
    } else {
      mNumber[i] = 0;
    }
  }
  printf("\r\n</tr>\r\n");
  // result td
  printf("<tr>\r\n");
  for (int i = 0; i < MAX_SERVER; i++) {
    if(mNumber[i] == 1){
      printf("<td valign=\"top\" id=\"m%d\"></td>", i);
    }
  }
  printf("\r\n</tr>\r\n");
  printf("</table>\r\n");
  printf("</font>\r\n");
  printf("</body>\r\n");
  printf("</html>\r\n");
}

int main(int argc, char *argv[],char *envp[] ) {
  printf("Content-type: text/html; charset=big5\n\n");
  char *query = getenv("QUERY_STRING");
  printf("query: %s<br/>", query);

  Server servers[MAX_SERVER];
  for (int i = 0; i < MAX_SERVER; i++) {
    initialize_server(&servers[i]);
  }

  sscanf(query, "h1=%[^&]&p1=%d&f1=%[^&]&sh1=%[^&]&sp1=%d&h2=%[^&]&p2=%d&f2=%[^&]&sh2=%[^&]&sp2=%d&h3=%[^&]&p3=%d&f3=%[^&]&sh3=%[^&]&sp3=%d&h4=%[^&]&p4=%d&f4=%[^&]&sh4=%[^&]&sp4=%d&h5=%[^&]&p5=%d&f5=%[^&]&sh5=%[^&]&sp5=%d",
                servers[0].ip, &servers[0].port, servers[0].fileName, servers[0].ssIP, &servers[0].ssPort,
                servers[1].ip, &servers[1].port, servers[1].fileName, servers[1].ssIP, &servers[1].ssPort,
                servers[2].ip, &servers[2].port, servers[2].fileName, servers[2].ssIP, &servers[2].ssPort,
                servers[3].ip, &servers[3].port, servers[3].fileName, servers[3].ssIP, &servers[3].ssPort,
                servers[4].ip, &servers[4].port, servers[4].fileName, servers[4].ssIP, &servers[4].ssPort);

  responseServer(servers);
  requestServer(servers);
}
